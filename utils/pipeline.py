import os
import sys
import re
import utils.configs, utils.samples
from utils.manager import Manager
from utils import environ
from utils import regex_helper as rh
from utils.files import extensionless
from snakemake.io import temp
import datetime
from collections import OrderedDict
from utils.strings import StringFormatter

class PipelineManager(Manager):
  """ """
  """
  Variables names in VARENV_NAMES of the form 'someVar' are fetched among the
  shell environment variables as '_PYPETTE_SOME_VAR' and declared as class objects.
  """
  VARENV_TAG = "_PYPETTE_"
  VARENV_NAMES = (
    'home', 'project', 'pipeName', 'pipeSnake', 'exeTime', 'exeDir', 'workdir',
    'clusterMntPoint', 'keepFilesRegex', 'keepLastBam', 'stats', 'strandedness',
    'cmdTargets'
  )

  TEMP_FILES = 'kept-temp-files.txt'

  def __init__(self, namespace, name="Default"):
    super(PipelineManager, self).__init__()
    environ.setTaggedVarEnvsAttrs(self, tag=self.__class__.VARENV_TAG)
    self.checkVarenvAttrs()
    self.namespace     = namespace
    self.modulesDir    = os.path.join(self.home, "modules")
    self.pipelinesDir  = os.path.join(self.home, "pipelines")
    self.params        = []
    self.cleanables    = []
    self.targets       = OrderedDict({})
    self.snakefiles    = []
    self.sampleManager = utils.samples.SamplesManager(self.pipeName, self.namespace)
    self.configManager = PipelineConfigManager(
                              config_prefix = self.pipeName,
                              namespace     = self.namespace)
    self.moduleDir     = ""
    self.updateNamespace()

    """ Required Config Files """
    self.configFiles    = ()

    """ Patterns for milestone rules. See function addMilestonesRules() """
    self.milestonesRules = {}

    """ Load internal default config """
    self._loadConfigFiles()

    """ Set default working dir """
    self.setDefaultWorkingDir()

    """ Set default jobs dir """
    self.checkJobsDir()

  def checkVarenvAttrs(self):
    for varenv in self.__class__.VARENV_NAMES:
      self.checkVarenvAttr(varenv)
    self.enforceListVarenvAttrs()

  def checkVarenvAttr(self, attr):
    assert hasattr(self, attr), f"Environment variable '{attr}' not found."

  def enforceListVarenvAttrs(self):
    if type(self.cmdTargets) == str:
      self.cmdTargets = [self.cmdTargets]

  @property
  def workflow(self):
    return self.namespace['workflow']

  @property
  def snakefile(self):
    """
    Returns the path to the given pipeline's snakefile.
    """
    return os.path.join(
      self.home, "pipelines", self.name, f"{self.name}.sk"
    )

  def updateNamespace(self):
    """
    Saves itself in the global namespace
    """
    self.namespace['pipeline_manager'] = self

  # ---------
  # Samples
  # ---------
  @property
  def samples(self):
    return self.sampleManager
 
  @property
  def sampleExtensions(self):
    return self.sampleManager.configManager.extensionsDelimiters

  def input(self, *args, **kwargs):
    """
    Allows more clarity to Snakemake input definitions
    """
    return lambda wildcards: self.samples.map(*args, file="samples/all/runs/{sample_run}/samples.csv", **wildcards, **kwargs)
 
  # ----------------
  # Temporary files
  # ----------------
  def temp(self, name):
    if self.isFileToKeep(name):
      self.updateTempFiles(name)
      return name
    if self.isBlacklistedTempRule():
      return name
    else:
      return temp(name)

  def isFileToKeep(self, name):
    try:
      if self.keepFilesRegex                              \
      and rh.isRegexInList(
               self.keepFilesRegex,
               [name,],
               **self.workflow._wildcard_constraints):
        return True
      else:
        return False
    except:
      self.log.error(f"That regexp won't do: '{self.keepFilesRegex}'")

  def loadedRule(self):
    """
    Returns the rule currently being loaded in the Snakemake workflow.
    """
    wfd    = self.workflow.__dict__
    ruleNb = wfd['rule_count']
    rules  = wfd['_rules']
    return list(rules)[ruleNb-1]

  def updateTempFiles(self, name):
    self.touchTempFilesFile()
    with open(self.tempFilesFile(), 'r+') as tempFiles:
      for tempFile in tempFiles:
        if name in tempFile:
          break
      else:
        tempFiles.write(f"{name}\n")

  def touchTempFilesFile(self):
    open(self.tempFilesFile(), 'a').close()
     
  def tempFilesFile(self):
    f = self.config.pipeline.tempFiles
    if not f:
      f = self.TEMP_FILES
    return f

  def isBlacklistedTempRule(self):
    """
    Says if the currently loaded rule is part of the ones which outputs should
    not be temporary.
    """
    if self.keepLastBam and \
       self.loadedRule() in self.milestoneBlacklistedTempRules('bam'):
      return True
    else:
      return False

  # -------------------------------------------------------------------------------
  # Milestones Rules Pattern
  #
  # It is possible to define milestone rules, such as 'fastq', 'bam', etc and
  # associate them with expected patterns. The purpose is to be able to recognize
  # the last pattern for each milestone from the expected command target in order  
  # to disable the temporality of the last rule of a milestone type. 
  # -------------------------------------------------------------------------------
  def addMilestonesRules(self, *milestonesRules):
    """
    Adds the given list of tuples (milestone, rulename, pattern) to pypette.

    Patterns can be associated to milestone rules as follow:
      pypette.addMilestonesRules(
        ('bam',   'ruleBam1', 'patternBa1'),
        ('bam',   'ruleBam2', 'patternBam2'),
        ('fastq', 'ruleFq1',  'patternFq1'))

    The final expected structure will be as follow:
      {
        'bam': [
           ('ruleBam1', 'patternBam1'),
           ('ruleBam2', 'patternBam2') ],
        'fastq': [
           ('ruleFq1', 'patternFq1'),
           ('ruleFq2', 'patternFq2'),]
      }

    """
    for milestoneRules in milestonesRules: 
      milestone, rulename, pattern = self.checkMilestoneRules(*milestoneRules)
      if milestone in self.milestonesRules.keys():
        self.milestonesRules[milestone].append((rulename, pattern))
      else:
        self.milestonesRules[milestone] = [(rulename, pattern)]
  
  def checkMilestoneRules(self, *milestoneRules):
    """
    Checks the unpacking of the :milestoneRules: matches the expected variables.
    Expects a :milestoneRules: tuple comprising a milestone name, its associated
    rulename and a target pattern as in the following example:

       checkMilestoneRules('bam', 'ruleBam1', 'patternBam1'), 

    """ 
    try:
      milestone, rulename, pattern = milestoneRules 
    except:
      self.log.error(f"""
        {sys.exc_info()}
        Could not correctly unpack a :milestone:, :rulename: and :pattern: from the given
        tuple: {tuple(milestoneRules)}. Please fix it in your milestone definition.
        Expected definition is of the form:
  
          pypette.addMilestonesRules(
            ('bam',   'ruleBam1', 'patternBa1' ),
            ('bam',   'ruleBam2', 'patternBam2'),
            ('fastq', 'ruleFq1',  'patternFq1' ))
  
      """)
      sys.exit()
    return milestone, rulename, pattern
    

  def milestoneBlacklistedTempRules(self, milestone):
    """
    Returns a list of rules for the specified :milestone: which output temporality
    has to be cancelled.
    """
    from functools import reduce
    def maxRulePos(r1, r2):
      """
      Returns the tuple which rule's position X is higher or equal to the other's Y.
      Expected inputs of the form:
        :r1: ('rule1', X)
        :r2: ('rule2', Y)
      """
      if r1[1] >= r2[1]:
        return r1
      else:
        return r2

    rulesPos = []
    if milestone not in self.milestonesRulesPosInTargets.keys():
      return rulesPos

    for targetIdx in range(len(self.cmdTargets)):
      rulesPosTarget = [
        (rule, pos[targetIdx])
        for rule, pos in self.milestonesRulesPosInTargets[milestone]
        if pos[targetIdx] ]

      rulePosTarget = reduce(maxRulePos, rulesPosTarget) if rulesPosTarget else None
      if rulePosTarget:
        rulesPos.append(rulePosTarget)

    return list(set([ rule for rule, pos in rulesPos ]))

  @property
  def milestonesRulesPosInTargets(self):
    """
    Parses the given patterns in milestone rules.
    Returns a milestone dict containing the rules and the position of their
    patterns in every target.
    Return is of the form:

      {
        'bam': [
           ('ruleBam1', (positionTarget1, positionTarget2)),
           ('ruleBam2', (positionTarget1, positionTarget2)),]
        'fastq': [
           ('ruleFq1', '(positionTarget1, positionTarget2)),
           ('ruleFq2', (positionTarget1, positionTarget2)) , ]
      }

    """
    ret = {}

    """ Get milestone rules """
    for milestone in self.milestonesRules.keys():
      for rulename, pattern in self.milestonesRules[milestone]:
        targetPos = self.patternPosInTargets(pattern)
  
        """ Populate dict """
        if milestone in ret.keys():
          ret[milestone].append((rulename, targetPos))
        else:
          ret[milestone] = [(rulename, targetPos)]
    return ret

  def patternPosInTargets(self, pattern):
    return tuple(self.patternPosInStr(pattern, target) for target in self.cmdTargets)

  def patternPosInStr(self, pattern, string):
    """
    Returns the :pattern: position found in the given :string:.
    Returns None if nothing found.
    :pattern: shoudl be a re.compile() object.
    """
    try:
      return re.compile(pattern).search(string).span()[0]
    except:
      return None

  # -----------------
  # Pipeline Config
  # -----------------
  @property
  def config(self):
    """
    Returns Snakemake's global config variable
    """
    return self.namespace['config']

  def configFromKeysString(self, string=""):
    """
    Retrieves the value of an addict from string.
    The addict's instance name is expected:
     - to be the first element split from the string.
     - to be in the globals.
    """
    keys = string.split('.')
    return self.configFromKeys(self.namespace[keys[0]], keys[1:])
   
  def configFromKeys(self, config, keys=[]):
    """
    Retrieves recursively the value of an addict from a list of keys.
    """
    if not keys:
      return config
    elif len(keys) > 1:
      return self.configFromKeys(config[keys[0]], keys[1:])
    else:
      return config[keys[0]]

  def missingConfigFiles(self):
    """
    Returns the required files that are missing.
    """
    return [ conf
             for conf in self.configFiles
             if not os.path.exists(conf)
             and not os.path.isfile(conf)
           ]

  def addConfigFiles(self, *args):
    self.configFiles = tuple(set( (*self.configFiles, *args) ))
 
  def loadConfigFiles(self):
    """ Check missing files """
    missingFiles = self.missingConfigFiles()
    if missingFiles:
      self.log.warning(f"Missing required files '{missingFiles}'")
    """ Load non missing config """
    for conf in self.configFiles:
      if conf not in missingFiles:
        self.configManager.load(conf)

  def _loadConfigFiles(self):
    """
    Loads the pipeline's internal config files
    """
    for conf in self._configFiles():
      self.configManager.load(conf)

  def _configFiles(self):
    """
    Returns the given pipeline's internal config files.
    """
    import glob
    ret = []
    for ext in self.configManager.extensions:
      ret.extend(
        glob.glob(f"{self.pipelinesDir}/{self.pipeName}/*{ext}"))
    return ret

  @property
  def pipelines(self):
    return [
      path
      for path in next(os.walk(self.pipelinesDir))[1]
      if not path.startswith('.')
    ]

  def defaultWorkingDir(self):
    return os.path.join(
      self.config.cluster.stdAnalysisDir,
      self.config.pipeline.outDir,
      self.project)

  def setDefaultWorkingDir(self):
    if not self.hasCustomDir():
      if not self.workdir:
        self.workdir = self.defaultWorkingDir()
      os.makedirs(self.workdir, exist_ok=True)
      os.chdir(self.workdir)
    self.log.info(f"Working directory set to '{os.getcwd()}'")

  def hasCustomDir(self):
    return self.workflow.workdir_init != self.exeDir

  # ------------------
  # Snakemake Scripts
  # ------------------
  def rscript(self, name):
    """
    Sources an R script, dealing with snakemake parameters.
    """
    # Set R Variables
    if "R_LIBS" not in os.environ:
      os.environ["R_LIBS"] = ""
    os.environ["R_LIBS"] = os.pathsep.join([
      os.environ["R_LIBS"],
      self.modulesDir]
    ).strip(os.pathsep)

    # Set Pipeline Variables for R scripts
    os.environ["_PYPETTE_SCRIPT"] = os.path.join(self.modulesDir, name)
    os.environ["_PYPETTE_MODULES"] = self.modulesDir
   
    return os.path.join(self.modulesDir, "core/script.R")

  
  # ------------
  # Snakefiles
  # ------------   
  def include(self, name, outDir=True, asWorkflow=""):
    """
    Includes the given file allowing to reflect the workflow of processes in the ouput dir.
    By default, sets the pipeline manager workdir to the module's basename if :outDir:.
    Concatenates the module's basename to workdir if :asWorkflow: is set (default).
    Example:
      :name: module3/module3.sk
      workdir = "module1/module2"
      Sets workdir to "module1/module2/module3" with :outDir: True and :asWorkflow: True
      Sets workdir to "module3" with :outDir: True and :asWorkflow: False.
      Doesn't touch workdir if :outDir: False
    """

    """ Set workdir"""
    if outDir:
      basename = extensionless(os.path.basename(name))
      if asWorkflow:
        self.workdir = os.path.join(self.workdir, asWorkflow)
      self.moduleDir = basename

    """ Include File """
    self.workflow.include(name)

  def includePipeline(self, name):
    self.include(os.path.join(self.pipelinesDir, name))

  def includeModule(self, name, *args, **kwargs):
    self.include(self.modulePath(name), **kwargs)

  def includeModules(self, *modules, withConfigFiles=False, **kwargs):
    """ Check required files """
    missingFiles = self.missingConfigFiles()
    if missingFiles and withConfigFiles:
      self.log.warning(f"Couldn't include modules '{modules}': They depend on the missing files '{missingFiles}'.")
    else:
      for module in modules:
        self.includeModule(module, **kwargs)

  def modulePath(self, name):
    """ Returns the module path in pypette directories """
    return os.path.join(self.modulesDir, name)

  def isModule(self, name):
    """ Checks if given {name} is in pypette modules """
    return os.path.isfile(self.modulePath(name))

  def _loadModule(self, name):
    pass

  def addModule(self, name):
    pass

  # -------------------------
  # Include Workflow Modules
  # -------------------------
  def includeWorkflow(self, *modules):
    """
    Includes all available workflow files related to the given list of workflow {modules}.
    Target files {module}.targ are loaded first. They should contain variables for the pipeline.
    Snakefiles {module}.sk are loaded afterwards. They should include Snakemake rules.A
    Ex: Including the workflow "fastq/trimming" and "fastq/adapters" will:
      - Load fastq/trimming.targ , fastq/adapters.targ
      - Cache fastq/trimming.sk   , fastq/adapters.sk
    """
    targets = [ f"{module}.targ" for module in modules ]
    skfiles = [ f"{module}.sk"   for module in modules ]
    self.includeWorkflowModules(*targets)
    self.snakefiles += skfiles

  def includeWorkflowModules(self, *modules, **kwargs):
    """ Includes the given module names if they exist. """
    self.includeModules(
      *list(
        [ module for module in modules if self.isModule(module) ]),
      **kwargs
    )

  def loadSnakefiles(self):
    self.includeWorkflowModules(*self.snakefiles, withConfigFiles=True)

  def loadWorkflow(self):
    self.formatAllTargets()
    self.loadSnakefiles()
   
  # ------------------
  # Fomatted Targets
  #
  # This features allows to compose flexible formattable strings.
  # Each can contain keywords from previously defined strings.
  # All strings will can be later evaluated at once.
  # String names are then defined in Snakemake namespace.
  # ------------------
  def addTargets(self, **kwargs):
    """ Save the given strings and their associated values """
    self.targets.update(kwargs)
    for key, val in kwargs.items():
      self.namespace[key] = val

  def formatTargets(self, **kwargs):
    """ Format and declare the given target dict. """
    for key, val in kwargs.items():
      self.formatTarget(key, val)

  def formatTarget(self, key, value):
    kwVals = { key: self.namespace[key] 
               for key in StringFormatter(value).keywords()
             }
    self.namespace[key] = value.format(**kwVals)

  def formatAllTargets(self):
    """ Formats all the saved targets """
    self.formatTargets(**self.targets)

  # -----------------------
  # Wildcards Constraints
  # -----------------------
  def updateWildcardConstraints(self, **wildcards):
    self.workflow.global_wildcard_constraints(**wildcards);
 
  # ------------
  # Parameters
  # ------------
  def setParams(self, *params):
    """
   
    """
    all_set = True
    for param in params:
      if not self.configFromKeysString(param):
        self.log.warning(f"Parameter '{param}' not found in configuration.")
        all_set = False
    if not all_set:
      raise

  def addParams(self, *params):
    """
    Adds the given parameters to a list.
    Each parameter is unique.
    """
    for param in params:
      self.addParam(param)
    self.params = list(set(self.params))

  def addParam(self, param):
    self.params.extend([param,])

  def areParamsOk(self):
    try:
      self.checkParams()
      return True
    except:
      return False

  def checkParams(self):
    toraise = False
    for param in self.params:
      if not self.configFromKeysString(param):
        #self.log.warning("Parameter '{}' not set in configuration.".format(param))
        toraise = True
    if toraise:
      raise

  # -----
  # Jobs
  # -----
  @property
  def jobExeBase(self):
    return f"{self.jobsExeDir}{os.path.sep}{self.jobExeTime()}_{self.config.pipeline.name}"

  @property
  def jobsExeDir(self):
    return f"jobs{os.path.sep}{self.exeTime}"

  def jobExeTime(self):
    return datetime.datetime.now().strftime('%H%M%S-%f')

  def checkJobsDir(self):
    os.makedirs(self.jobsExeDir, exist_ok=True)

  @property
  def jobName(self):
    """
    Returns the default job name.
    Truncates the name to 13 chars as the old PBS jobs submission fails when
    the requested job name is over 13 characters.
    """
    return f"{self.config.pipeline.name}-pypette"[:12]

  # ---------------
  # Cleaning files
  # ---------------
  def toClean(self, *patterns):
    """
    Adds the given patterns to the list of files to clean.
    """
    self.cleanables.extend([*patterns])

class PipelineConfigManager(utils.configs.ConfigManagerTemplate):
  def __init__(self, *args, **kwargs):
    super(PipelineConfigManager, self).__init__('config', *args, **kwargs)

  @property
  def extensions(self):
    return ('.yaml', '.json',)

  @property
  def configfileBase(self):
    return "config"

  def load(self, file):
    """
    loads the given snakemake configuration file.
    Updates and converts it to an Addict.
    """
    print(type(file),file)
    self.namespace['workflow'].configfile(file)
    self.updateNamespace()

  def updateNamespace(self):
    """
    Converts the config from a regular Python dictionnary into an addict's.
    """
    import addict
    self.namespace['config'] = addict.Dict(self.namespace['config'])
