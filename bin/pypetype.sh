# bash
source pypette.sh
pypette::setManual pypetype::manual

# ----------
# Workflow
# ----------
pypetype::runFlow() {
  pypetype::initParams
  pypetype::parseParams "$@"
  pypetype::checkParams
  pypetype::exportVarenvs
  pypetype::execPipeline
}

pypetype::exportVarenvs() {
  export FORCE
  export DEBUG
  export VERBOSE
  pypette::exportExecVarenv "CMD_TARGETS"
}

# --------------
# CLI & Parsing
# --------------
pypetype__paramsMandatory=(PROJECT CMD_TARGETS)
pypetype::manual() {
  cat << eol

  PYPETTE v$(pypette::version)

  DESCRIPTION
      Launches the $(pypetype::pipeline) pipeline to produce TARGET files for the given PROJECT.

  USAGE
      $ $0 \ 
          TARGET [TARGET ...]                            \ 
          [ --project|-p PROJECT ]                       \ 
          [ --strandedness $(pypette::strandednessOpts)    \ 
          [ --cluster-rules FILE ]                       \ 
          [ --no-cluster ]                               \ 
          [ --cores|--jobs|-j N ]                        \ 
          [ --force ]                                    \ 
          [ --outdir ]                                   \ 
          [ --snake-opts SNAKE_OPTION ...]               \ 
          [ --noversion ]                                \ 
          [ --stats ]                                    \ 
          [ --keep-files-regex REGEX ]                   \ 
          [ --keep-last-bam ]                            \ 
          [ --debug ]                                    \ 
          [ --verbose ]

  OPTIONS
      TARGET
          Targets to build. May be rules or files.

      -p|--project
          Name of the project to process. Default: "${PROJECT}".

      --strandedness $(pypette::strandednessOpts)
          Forces the strandedness to the pipeline.

      -c|--cluster-rules
          Yaml file with all the pipeline's rules for cluster execution.
          Default is $(pypetype::clusterRulesDft).

      --no-cluster
          Executes every job on this machine. Disables the use of job schedulers.
          Cluster options are not forwarded to Snakemake.
          Disables the --scheduler option.

      --cores|--jobs|-j
          Max number of cores in parallel.

      -f|--force
          Forces the generation of the TARGET.

      -o|--outdir
          The directory where to write output results.
 
      --snake-opts
          Lists options to pass to Snakemake
 
      --scheduler $(pypette::schedulerOpts)
          Forces the use of the given job scheduler.
          To force the execution of jobs locally without any scheduler, use the --no-cluster option instead.
          Disabled by the --no-cluster option.
          Disabled by the --debug option.

      --noversion
          Updates the last version of pypette run on the PROJECT.
          If PROJECT has been run with a previous version, Snakemake might run workflows from scratch. 

      --stats
          Adds stats file for each target. To be used in a testing environment node
          where no other job is influencing the resource workload.

      -k|--keep-files-regex
          The regex pattern of the temporary files to keep (ex.: '.*merged/.*bam').

      --keep-last-bam
          Keeps the last bam from the workflow of all targets.

      --debug
          Executes the pipeline in debug mode.
          Jobs are not executed, targets are produced empty on the local machine.
          Disabled the --scheduler option.
          Not recommended for analysis projects.

      -v|--verbose
          Makes this command verbose.

      -h|--help
          Displays this help manual.


eol
}

# -----------
# Parameters
# -----------
pypetype::initParams() {
  WORKDIR="$(pwd)"
  PROJECT=$(basename "$WORKDIR")
  STRANDEDNESS=""
  SCHEDULER=""
  CMD_TARGETS=()
  SNAKE_OPTIONS=()
  USE_CLUSTER=true
  MAX_CORES="1"
  NOVERSION=""
  STATS=""
}

pypetype::parseParams() {
  while [ $# -ge 1 ]; do
    case "$1" in
      -p|--project)
        PROJECT="$2" && shift
        ;;

      --strandedness)
        STRANDEDNESS="$2" && shift
        ;;

      -c|--cluster-rules)
        CLUSTER_RULES="$2" && shift
        ;;

      --no-cluster)
        USE_CLUSTER=false
        ;;

      --cores|--jobs|-j)
        MAX_CORES="$2" && shift
        ;;

      -f|--force)
        FORCE=true
        ;;

      -o|--outdir)
        WORKDIR=$(pypette::fullPath "$2") && shift
        ;;

      --snake-opts)
        SNAKE_OPTIONS+=("$2") && shift
        ;;

      --scheduler)
        SCHEDULER="$2" && shift
        ;;

      --noversion)
        NOVERSION=true
        ;;

      --stats)
        STATS=true
        ;;

      -k|--keep-files-regex)
        KEEP_FILES_REGEX="$2" && shift
        ;;

      --keep-last-bam)
        KEEP_LAST_BAM=true
        ;;

      --debug)
        DEBUG=true
        ;;

      -h|--help)
        pypetype::manual && exit
        ;;

      -v|--verbose)
        VERBOSE=true
        ;;

      -*)
        pypette::errorUnrecOpt "$1"
        ;;

      *)
        CMD_TARGETS+=($1)
        ;;

    esac
    shift
  done
}

pypetype::checkParams() {
  pypette::requireParams ${pypetype__paramsMandatory[@]}
  pypette::checkScheduler
}

# -----------------------
# Pipeline Type Specific
# -----------------------
pypetype::pipeline() {
  pypetype::name $(pypette::extless $(pypette::cmdName))
}

pypetype::name() {
  printf "${1##$(pypetype::cmdPrefix)}"
}

pypetype::cmdPrefix() {
  printf "pypette-"
}

# ---------------------
# Pipeline Execution
# ---------------------
pypetype::execPipeline() {
  pypette::infecho "\$ $(pypetype::cmdPipeline)"
  eval $(pypetype::cmdPipeline)
}

pypetype::cmdPipeline() {
  cat << eol | tr '\n' ' '
    $(pypette::cmdDir)/pypette
   -p $(pypetype::pipeline)
   --project "$PROJECT"
   ${STRANDEDNESS:+--strandedness ${STRANDEDNESS}}
   ${WORKDIR:+--outdir ${WORKDIR}}
   ${KEEP_FILES_REGEX:+--keep-files-regex ${KEEP_FILES_REGEX}}
   ${KEEP_LAST_BAM:+--keep-last-bam}
   ${NOVERSION:+--noversion}
   ${STATS:+--stats}
   --snakemake "$(pypetype::smkParams)"
eol
}

# -------------------
# Snakemake Options
# -------------------
pypetype::smkParams() {
   echo "${CMD_TARGETS[@]} $(pypetype::smkOptions)"
}

pypetype::smkOptions() {
  cat << eol
  $(pypetype::smkBaseOpts)
  $(pypetype::smkClusterOpts)
eol
}

pypetype::smkBaseOpts() {
  cat << eol
  ${MAX_CORES:+--jobs $MAX_CORES}
  --latency-wait 90
  --rerun-incomplete
  ${SNAKE_OPTIONS:+${SNAKE_OPTIONS[@]}}
  ${FORCE:+--force}
  ${DEBUG:+--config debug=True}
eol
}

pypetype::smkClusterOpts() {
  ! pypetype::smkCanUseClusterOptions                      \
  || pypetype::smkClusterOptsStr 
}

pypetype::smkCanUseClusterOptions() {
   ! pypette::isParamGiven "DEBUG" \
  && $USE_CLUSTER
}

pypetype::smkClusterOptsStr() {
  #
  # Returns a cluster config string for Snakemake if a scheduler is either set or found by default.
  #
  local scheduler=$(pypetype::schedulerToUse) schedulerCmd
  ! [ -z ${scheduler:+x} ] || return 0
  schedulerCmd=$(pypette::capWord <<< "$scheduler")
  cat << eol
  --cluster-config $(pypetype::clusterRules)
  --cluster \"$(pypetype::cmd${schedulerCmd})\"
eol
}

pypetype::schedulerToUse() {
  #
  # Decides which job scheduler to use.
  # If not given in option, uses the first one found on this machine.
  #
  if pypette::isParamGiven "SCHEDULER"; then
    printf $SCHEDULER
  else
    pypetype::schedulerDft
  fi
}

pypetype::schedulerDft() {
  #
  # Returns the first default job scheduler found. Empty if none.
  #
  pypetype::hasSbatch || pypetype::hasQsub || :
}

pypetype::hasSbatch() {
  #
  # Checks for SLURM job scheduler.
  #
  type sbatch &>/dev/null && printf 'sbatch'
}

pypetype::hasQsub() {
  #
  # Checks for PBS job scheduler.
  #
  type qsub &>/dev/null && printf 'qsub'
}

pypetype::cmdQsub() {
  cat << "eol"
  qsub
    -V
    -N {cluster.name}
    -l select={cluster.select}:ncpus={cluster.ncpus}:mem={cluster.mem}
    -o {cluster.out}
    -e {cluster.err}
eol
}

pypetype::cmdSbatch() {
  cat << "eol"
  sbatch
    --export=ALL
    -A ${USER}
    --job-name {cluster.name}
    -N {cluster.select}
    -c {cluster.ncpus}
    --mem={cluster.mem}
    --time=INFINITE
    -o {cluster.out}
    -e {cluster.err}
eol
}


# --------------------
# Cluster Ressources
# --------------------
pypetype::clusterRules() {
  printf "${CLUSTER_RULES:-$(pypetype::clusterRulesDft)}"
}

pypetype::clusterRulesDft() {
  pypette::fullPath "$(pypetype::clusterRulesDftTemplate)"
}

pypetype::clusterRulesDftTemplate() {
  printf "$(pypette::cmdDir)/../pipelines/$(pypetype::pipeline)/cluster-rules.yaml"
}
