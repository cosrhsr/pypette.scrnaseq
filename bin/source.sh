#!/usr/bin/env bash
_dir=$(readlink -f $(dirname "${BASH_SOURCE[0]}")) 
_curdir=$(pwd)
export PATH="${_dir}:${PATH}"

# Sugar-sourcing for cli-pipette
alias clip-start='source clip'
