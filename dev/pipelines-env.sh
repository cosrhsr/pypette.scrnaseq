# bash

# ---------------
# Conda Env Mode
# ---------------
function checkEnvMode() {
  export ENV_MODE="${1:-}"
  pypette::checkEnvMode
  # Ensure prod env name has no suffix.
  [ "$ENV_MODE" != 'production' ] || ENV_MODE=''
}

# -------------------
# Conda Env Commands
# -------------------
function exportCondaEnv() {
  conda env export -n $1
}

function createCondaEnv() {
  conda env create --file "$1"
}

# -------------
# Conda Files
# -------------
function pipelineRequFile() {
  printf "$(pypette::pipelineDir ${1})/conda.yaml"
}

function pipelineRequFileTmp() {
  printf "$(pypette::pipelineDir ${1})/conda.installed.yaml"
}

function pipelineEnvName() {
  printf "pypette-${1}${ENV_MODE:+-$ENV_MODE}"
}

# ----------------
# Conda Commands
# ----------------
function exportPipelineEnv() {
  local envName=$(pipelineEnvName $1)
  printf "> Exporting pipeline env '$envName'..." >&2
  exportCondaEnv "$envName" \
    && printf " OK\n" >&2   \
    || printf " KO\n" >&2
}

function createPipelineEnv() {
  local pipeline envName
  pipeline="$1"
  envName=$(pipelineEnvName $pipeline)
  printf "> Creating pipeline env '$envName'...\n" >&2
  createPipelineRequFileTmp "$pipeline"
  createCondaEnv $(pipelineRequFileTmp $pipeline) \
    && printf " OK\n" >&2                         \
    || printf " KO\n" >&2
}

function createPipelineRequFileTmp() {
  local pipeline envName pipelineRequFile pipelineRequFileTmp
  pipeline="$1" 
  envName=$(pipelineEnvName $pipeline)
  pipelineRequFile=$(pipelineRequFile $pipeline)
  pipelineRequFileTmp=$(pipelineRequFileTmp $pipeline)
  cat $pipelineRequFile | condaDepsPatchless | updateEnvName $pipeline > $pipelineRequFileTmp
  pipDeps            < $pipelineRequFile >> $pipelineRequFileTmp
}

function rmPipelineEnv() {
  local pipeline envName
  pipeline="$1" 
  envName=$(pipelineEnvName $pipeline)
  conda env remove -n $envName
}

function updatePipelineEnv() {
  local pipeline="$1"
  rmPipelineEnv "$pipeline"
  createPipelineEnv "$pipeline"
}

# --------------------
# Conda Requirements
# --------------------
function updateEnvName() {
  cat /dev/stdin | sed '1 s/^.*$/name: '$(pipelineEnvName $1)'/'
}

function condaDepsPatchless() {
  #
  # Removes patch version from conda dependencies.
  # Reads from STDIN.
  #
  cat /dev/stdin | condaDeps | rmCondaPatches
}

function filterDepsLines() {
  #
  # Filters conda deps lines.
  # Reads from STDIN.
  #
  cat /dev/stdin | grep -v '^prefix' | sed '/^$/d'
}

function condaDeps() {
  #
  # Returns conda dependencies.
  # Reads from STDIN.
  #
  local data pipDepsLines pipDepsStartingLine
  data=$(cat /dev/stdin | filterDepsLines)
  pipDepsNbLines=$(pipDepsNbLines <<< "$data")
  head -n -$pipDepsNbLines <<< "$data"
}

function pipDeps() {
  #
  # Returns pip dependencies.
  # Reads from STDIN.
  #
  local data pipDepsNbLines
  data=$(cat /dev/stdin | filterDepsLines)
  pipDepsNbLines=$(pipDepsNbLines <<< "$data")
  if [ $pipDepsNbLines -gt 0 ]; then
    tail -n ${pipDepsNbLines} <<< "$data"
  else
    :
  fi
}

function pipDepsNbLines() {
  #
  # Returns number of lines in pip dependencies.
  # Reads from STDIN.
  #
  local data pipStart pipLines
  data=$(cat /dev/stdin)
  pipStart=$(pipDepsLineStart <<< $data)
  if [ -z ${pipStart:+x} ]; then
    pipLines=0
  else
    pipLines=$(
      tail -n +${pipStart} <<< "$data" \
      | grep -n '^    - '              \
      | wc -l  )
  fi
  printf $((pipLines+1))
}

function pipDepsLineStart() {
  #
  # Returns line where pip dependencies start.
  # Reads from STDIN.
  #
  cat /dev/stdin | grep -n '^  - pip:' | cut -d: -f1
}

function pipDepsGrepPattern() {
  cat /dev/stdin | grep -n '^    - '
}

function rmCondaPatches() {
  #
  # Removes patch versions from conda dependencies.
  # Reads from STDIN.
  #
  cat /dev/stdin | cut -d= -f1,2
}
