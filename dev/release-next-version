#!/usr/bin/env bash
set -euf -o pipefail
script=$(readlink -f "${BASH_SOURCE[0]}")
pypette=$(readlink -f $(dirname $(dirname "${BASH_SOURCE[0]}")))
source "${pypette}/bin/source.sh"
source pypette.sh

RELEASE_TYPES=('major' 'minor' 'patch')
RELEASE_CMDS=('stage' 'apply' 'release' 'revert' 'reset')
RELEASE_CMD=''
RELEASE_TYPE=''
RELEASE_TMP_FILE='.release'
MAJOR=0
MINOR=1
PATCH=2
BRANCH_DEV="develop"
BRANCH_PROD="master"
VERSION_FILE="version.txt"
VERSION_RELEASE_FILE="version-release.txt"
VERSIONS_UPDATES_FILE="versions-updates.txt"

msg()        { echo -e $@ | sed 's/  / /g' >&2; }
msg-info()   { msg "INFO: $@"  ; }
msg-error()  { msg "ERROR: $@" ; }
exit-error() { msg-error $@; exit 1; }

manual ()
{
  cat << eol >&2

  DESCRIPTION
      Creates the next release incrementing the current version number.

      Current version number is found in the file 'version.txt' and should
      follow the sementic versioning patter MAJOR.MINOR.PATCH, each of them 
      being a number. For more info, see https://semver.org/.

      Staging releases are checked out from the '${BRANCH_DEV}' branch. Make sure
      to call the command from that branch.

  USAGE
      $ $0 stage [TYPE] | apply | release | revert | reset

  COMMANDS
      stage [TYPE]
          Checks out a new branch from '$BRANCH_DEV' to test and approve the current release. 
          TYPE: one among '${RELEASE_TYPES[@]}'. Default 'minor'.

      apply
          Applies the version modifications on the release branch.

      release
          Approves the changes and merges the staging branch with the '$BRANCH_DEV' and '$BRANCH_PROD',
          ready to be deployed in production.
   
      revert
          Cancels the changes in the staging branch and goes back to the '$BRANCH_DEV' branch. 
          It does include the 'reset' command.

      reset
          Resets release configuration in case something went wrong. It does not do a revert,
          though it removes temporary files used for the release.

  OPTIONS
      -h|--help
          Shows this manual.



eol
}

parse-params ()
{
  while [ $# -ge 1 ]; do
    case "$1" in
      -h|--help)
        manual
        exit 0
        ;;
      *)
        if [ ${#RELEASE_CMD} -eq 0 ]; then
          RELEASE_CMD="$1"
        else
          if [ ${#RELEASE_TYPE} -eq 0 ]; then
            RELEASE_TYPE="$1"
          else
            msg-error "Too many options given. Please see the --help option."
            return 1
          fi
        fi
        ;;
    esac
    shift
  done
}

proceed? ()
{
  read -p "Proceed? [y/n] " input
  tr '[:upper:]' '[:lower:]' <<< " $input " | grep -qs " y "
  printf "\n"
}

current-branch ()
{
  git rev-parse --abbrev-ref HEAD
}

# --------
# Dev Branch
# ------------
dev-version ()
{
  git show "${BRANCH_DEV}:${VERSION_FILE}"
}

dev-version-nb ()
{
  dev-version | sed 's/^v//'
}

dev-version-types-nb ()
{
  dev-version-nb | tr '.' ' '
}

# --------
# Release
# --------
release-info ()
{
  msg-info "Current Version: $(dev-version)"
  msg-info "Release Type   : $RELEASE_TYPE"
  msg-info "Next Version   : $(release-version)"
}

release-check-params ()
{
  release-check-cmd
  release-check-type
}

release-check-cmd ()
{
  if grep -qs " $RELEASE_CMD " <<< " ${RELEASE_CMDS[@]} "; then
    msg-info "Command: $RELEASE_CMD"
  else
    msg-error "
      Unexpected command '$RELEASE_CMD'. 
      Choose one among '${RELEASE_CMDS[@]}'.
    "
    return 1
  fi
}

release-check-type ()
{
  # TYPE only with 'stage' command
  if [ "$RELEASE_CMD" = 'stage' ]; then
    # Check release type
    if [ ${#RELEASE_TYPE} -eq 0 ]; then
      RELEASE_TYPE='minor'
    else
      :
    fi
    if grep -qs " $RELEASE_TYPE " <<< " ${RELEASE_TYPES[@]} "; then
      if [ -f "$RELEASE_TMP_FILE" ]; then
        msg-error "
          It appears a release/hotfix branch already exists. Release it or revert it before creating another.
          If it is not the case, you can simply use the 'reset' command. Please check the --help option.
        "
        return 1
      else
        release-set-tmp
      fi
    else
      msg-error "
        Unexpected release type '$RELEASE_TYPE'. 
        Choose one among '${RELEASE_TYPES[@]}'.
      "
      return 1
    fi
  else
    # Other commands accept no parameter.
    if [ ${#RELEASE_TYPE} -eq 0 ]; then
      RELEASE_TYPE=$(release-read-type)
    else
      msg-error "
        TYPE parameter can be given only with the 'stage' command. Please check the --help option.
      "
      return 1
    fi
  fi
}

release-read-type ()
{
  head -n 1 "$RELEASE_TMP_FILE" 2> /dev/null \
  || exit-error "
       It seems this branch as not been staged yet. Please check the --help option.
     "
}

release-set-tmp ()
{
  printf "$RELEASE_TYPE\n" > "$RELEASE_TMP_FILE"
}

release-rm-tmp ()
{
  rm "$RELEASE_TMP_FILE"
}

release-version-nb ()
{
  local versions
  if [ "$RELEASE_CMD" = 'release' ]; then
    if [ -f "$RELEASE_TMP_FILE" ]; then
      versions=( $(release-next-version $(dev-version-types-nb)) )
    else
      versions=( $(dev-version-types-nb) )
    fi
  else
    versions=( $(release-next-version $(dev-version-types-nb)) )
  fi
  tr ' ' '.' <<< "${versions[@]}"
}

release-version ()
{
  printf "v$(release-version-nb)"
}

release-next-version ()
{
  local versions=($@)
  case $RELEASE_TYPE in
    "major")
      versions[$MAJOR]=$(( versions[$MAJOR] + 1 ))
      versions[$MINOR]=0
      versions[$PATCH]=0
      ;;
    "minor")
      versions[$MINOR]=$(( versions[$MINOR] + 1 ))
      versions[$PATCH]=0
      ;;
    "patch")
      versions[$PATCH]=$(( versions[$PATCH] + 1 ))
      ;;
  esac
  echo ${versions[@]}
}

release-summary ()
{
  msg-info "Releasing version.\n"
  cat << eol
The following changes will be applied: 
  - Merge this release branch into the $BRANCH_PROD branch;
  - Tag the $BRANCH_PROD branch;
  - Merge this release branch into the $BRANCH_DEV branch;
  - Update the branches upstream.

eol
}

release-merge-stage ()
{
  release-merge-branch "$BRANCH_PROD"
  release-tag-branch
  release-merge-branch "$BRANCH_DEV"
}

release-merge-branch ()
{
  msg-info "Merging '$(stage-branch-name)' into '$1'"
  git checkout "$1"
  git pull origin "$1"
  git merge --no-edit --no-ff "$(stage-branch-name)" "$1"
}

release-tag-branch ()
{
  msg-info "Adding tag $(release-version) to branch $(current-branch)"
  git tag -a $(release-version) -m "Release $(release-version)"
}

release-push-upstream ()
{
  git push origin "$BRANCH_PROD" --follow-tags
  git push origin "$BRANCH_DEV"
}

release-leave-staging ()
{
  git checkout "$BRANCH_DEV"
  git branch -d "$(stage-branch-name)"
}

# ---------
# Staging
# ---------
#trap undo-staging EXIT SIGKILL

stage-check-branch ()
{
  local branch=$(current-branch)
  if [ "$branch" != "$BRANCH_DEV" ]; then
    msg-error "
      The command has to be run inside the '${BRANCH_DEV}' branch.
      Found branch '$branch'.
    "
    release-rm-tmp 
    exit 1
  else
    :
  fi
}

stage-branch-name ()
{
  printf "$(stage-branch-type)/$(release-version)\n"
}

stage-branch-type ()
{
  local type
  if [ "$RELEASE_TYPE" = 'patch' ]; then
    type='hotfix'
  else
    type='release'
  fi
  printf "${type}\n"
}

stage-checkout-branch ()
{
  git checkout -b $(stage-branch-name) ${BRANCH_DEV}
}

stage-undo ()
{
  git checkout ${BRANCH_DEV}
  msg-info "Checked out branch ${BRANCH_DEV}."
  git branch -d $(stage-branch-name)
  msg-info "Removed branch $(stage-branch-name)"
}

stage-instructions ()
{
  grep '^#' "$(pypette::homeDir)/$VERSION_RELEASE_FILE" || true
}

stage-current-features ()
{
  msg-info "Current features are:"
  stage-features
}

stage-features ()
{
  grep -v '^#' "$(pypette::homeDir)/$VERSION_RELEASE_FILE" \
  | sed -e '/^$/d'                                         \
        -e 's/^[ ]*\*/\*/'                                 \
  || true
}

stage-removed-features ()
{
  git diff "$VERSION_RELEASE_FILE" \
  | grep -v '^-[-]+'               \
  | grep '^-[ ]*\*'                \
  | sed -e 's/^-[ ]*\*/\*/'        \
        -e '/^$/d'                 \
  || true
}

# ---------------
# Apply Release
# ---------------
apply-summary ()
{
  msg-info "Applying changes.\n"
  cat << eol
The following changes will be applied: 
  - Set the release version to $(release-version)
  - Add the following features:
$(stage-features | sed 's/^/    /')
  - Exclude the following features:
$(stage-removed-features | sed 's/^/    /')
  - Commit all changes.

If you agree to proceed, the next step would be to check the commit and release the branch with the command:
  > $0 release

eol
}

apply-update-version ()
{
  printf "$(release-version)" > "$VERSION_FILE"
}

apply-features ()
{
  apply-new-features
  apply-removed-features
  apply-feature-changes
}

apply-new-features ()
{
  apply-versions-updates | cat - "$VERSIONS_UPDATES_FILE" > "${VERSIONS_UPDATES_FILE}.tmp"
  mv "${VERSIONS_UPDATES_FILE}.tmp" "${VERSIONS_UPDATES_FILE}"
}

apply-versions-updates ()
{
  apply-version-update-header
  stage-features
  printf '\n'
}

apply-version-update-header ()
{
  printf "## $(release-version) - *$(date +%d/%m/%Y)*\n"
}

apply-removed-features ()
{
  # Remove all features and empty lines
  sed -e '/^[^#]/d' -e '/^$/d' "$VERSION_RELEASE_FILE" > "${VERSION_RELEASE_FILE}.tmp"
  # Add removed features back
  stage-removed-features >> "${VERSION_RELEASE_FILE}.tmp"
  # Update file
  mv "${VERSION_RELEASE_FILE}.tmp" "$VERSION_RELEASE_FILE"
}

apply-feature-changes ()
{
  git add "$VERSION_FILE" "$VERSION_RELEASE_FILE" "$VERSIONS_UPDATES_FILE"
  git commit -m "[Release] Version $(release-version)"
}

# --------
# Revert
# --------
revert-files ()
{
  local files=("$VERSION_FILE" "$VERSION_RELEASE_FILE" "$VERSIONS_UPDATES_FILE")
  git restore --staged "${files[@]}"
  git restore          "${files[@]}"
}

revert-branch ()
{
  git checkout "$BRANCH_DEV"
  git branch -D $(stage-branch-name)
}

revert-summary ()
{
  msg-info "Reverting release.\n"
  cat << eol
The following changes will be applied: 
  - Changes to version files will be restored;
  - Branch $BRANCH_DEV will be checked out;
  - Release branch $(stage-branch-name) will be removed, all manual commits associated will be lost!!

eol
}

# -----------------
# Release Commands
# -----------------
release-stage ()
{
  stage-check-branch
  stage-checkout-branch
  stage-instructions
  stage-current-features
}

release-apply ()
{
  apply-summary
  proceed?
  apply-update-version
  apply-features
}

release-revert ()
{
  revert-summary
  proceed?
  revert-files
  revert-branch
  release-rm-tmp
}

release-release ()
{
  release-summary
  proceed?
  release-merge-stage
  release-push-upstream
  release-rm-tmp
  release-leave-staging
}

release-reset()
{
  release-rm-tmp
  msg-info "Release resetted."
}

# ----------------
# Execution Flow
# ----------------
parse-params $@
release-check-params
release-info
release-${RELEASE_CMD}
