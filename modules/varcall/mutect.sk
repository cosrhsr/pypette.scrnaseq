pypette.includeModules(
  "core/lang.py")

include: "mutect.py"

rule varcall__mutect2_sample:
  input:
    tumor_bam = f"{bam__sampleTarget}.bam",
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict()
  output:
    vcf = f"{vcf__sampleTarget}.mutect2.vcf.gz"
  run:
    if varcall__selectMatch({input.tumor_bam}) == None:
      cmd = f"""
        echo "This is a placeholder file for non-tumor samples." > {output.vcf}
      """
    else:
      normal_bam = varcall__selectMatch({input.tumor_bam})[0]
      normal_sample = varcall__selectMatch({input.tumor_bam})[1]
      gatkDir       = annot__gatkDir(sharedDir=True)
      genome_name = project__speciesGenomeName()
      if genome_name == "hs37d5":
          genome_name = "hg19"
      germline_resource = f"af-only-gnomad.{genome_name}.vcf.gz"
      ponormal = str(pypette.config.pipeline.modules.varcall.ponOptions)
      if not pypette.config.pipeline.modules.varcall.ponOptions:
          ponormal = ''
      else:
          ponormal = '--panel-of-normals ' + ponormal  # i.e. pypette.config.modules.varcall.pipeline.pon = "pon.vcf.gz"
      cmd = f"""
        gatk4 Mutect2 \
         -R {input.ref_genome_fa} \
         -I {input.tumor_bam} \
         -I {normal_bam} \
         -normal {normal_sample} \
         {ponormal} \
         --germline-resource {gatkDir}/{germline_resource} \
         -O {output.vcf}
      """
    exshell(**vars())

rule varcall__mutect2_filter:
  input:
        ref_genome_fa = lambda x: genome__ucscFasta(),
        ref_genome_fai = lambda x: genome__ucscFastaIdx(),
        ref_genome_dict = lambda x: genome__ucscFastaDict(),
        tumor_bam = f"{bam__sampleTarget}.bam",
        vcf = f"{vcf__sampleTarget}.mutect2.vcf.gz"
  output:
        vcf = f"{vcf__sampleTarget}.mutect2.filtered.vcf.gz"
  run:
    if varcall__selectMatch({input.tumor_bam}) == None:
      cmd = f"""
        echo "This is a placeholder file for non-tumor samples." > {output.vcf}
      """
    else:
      cmd = f"""
          gatk4 FilterMutectCalls \
          -R {input.ref_genome_fa} \
          -V {input.vcf} \
          -O {output.vcf}
      """
    exshell(**vars())

# We strongly recommend that you download/use the VEP Cache version which corresponds to your Ensembl VEP installation
rule varcall__mutect2_vep:
  input:
        tumor_bam = f"{bam__sampleTarget}.bam",
        vcf = f"{vcf__sampleTarget}.mutect2.filtered.vcf.gz"
  output:
        vcf = f"{vcf__sampleTarget}.mutect2.filtered.vep.vcf.gz"
  run:
    annotation_dir = annot__dir(sharedDir=True) + "/VEP-cache/" # put datasources files here
    # options must be put in config file with commas without spaces
    vep_options_str = str(pypette.config.pipeline.modules.varcall.vepOptions)
    vep_options = '--' + ' --'.join(vep_options_str.split(','))
    if varcall__selectMatch({input.tumor_bam}) == None:
      cmd = f"""
        echo "This is a placeholder file for non-tumor samples." > {output.vcf}
      """
    else:
      cmd = f"""
        vep --offline --cache -i {input.vcf} -o {output.vcf} {vep_options} --dir_cache {annotation_dir} --vcf --compress_output bgzip
      """
    exshell(**vars())

rule varcall__mutect2_vcf2maf:
  input:
        tumor_bam = f"{bam__sampleTarget}.bam",
        vcf = f"{vcf__sampleTarget}.mutect2.filtered.vep.vcf.gz",
        ref_genome_fa = lambda x: genome__ucscFasta()
  output:
        maf = f"{vcf__sampleTarget}.mutect2.filtered.vep.maf"
  params:
        vcf_tmp = f"{vcf__sampleTarget}.mutect2.filtered.vep.vcf"
  run:
    if varcall__selectMatch({input.tumor_bam}) == None:
      cmd = f"""
        echo "This is a placeholder file for non-tumor samples." > {output.maf}
      """
    else:
      vcf_tmp = f"{vcf__sampleTarget}.mutect2.filtered.vep.vcf"
      annotation_dir = annot__dir(sharedDir=True) + "/VEP-cache/" # put datasources files here
      genome_name = project__speciesGenomeName()
      normal_sample = varcall__selectMatch({input.tumor_bam})[1]
      cmd = f"""
      zcat {input.vcf} > {params.vcf_tmp}
      vcf2maf.pl \
       --ref-fasta {input.ref_genome_fa} \
       --inhibit-vep \
       --input-vcf {params.vcf_tmp} \
       --output-maf {output.maf} \
       --tumor-id {wildcards.sample_name} \
       --normal-id {normal_sample} \
       --filter-vcf 0
      """
    exshell(**vars())

rule varcall__mutect2_normal:
  input:
    normal_bam = f"{bam__sampleTarget}.bam",
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict()
  output:
    vcf = f"{vcf__sampleTarget}.mutect2.normal.vcf.gz"
  run:
    genome_name = project__speciesGenomeName()
    if genome_name == "hs37d5":
      genome_name = "hg19"
    if varcall__normalFilter(input.normal_bam.split(' ')):
      cmd = f"""
        gatk4 Mutect2 \
         -R {input.ref_genome_fa} \
         -I {input.normal_bam} \
         --max-mnp-distance 0 \
         -O {output.vcf}
      """
    else:
      cmd = f"""
        touch {output.vcf}
      """
    exshell(**vars())

rule varcall__pon_pre:
  input:
    normal_vcfs_pre = pypette.input(
             f"{vcf__sampleTarget}.mutect2.normal.vcf.gz",
             derefKwargs = ['sample_name',]),
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict(),
    ref_genome_list = lambda x: genome__ucscFastaScaffoldsList()
  output:
    pon_db = directory(f"{vcf__pipeTarget}/all.pon_db")
  run:
    normal_vcfs = '-V ' + ' -V '.join(varcall__normalFilter(map(str, input.normal_vcfs_pre)))
    cmd = f"""
      gatk4 GenomicsDBImport \
       -R {input.ref_genome_fa} \
       -L {input.ref_genome_list} \
       --genomicsdb-workspace-path {output.pon_db} \
       {normal_vcfs}
    """
    exshell(**vars())

rule varcall__pon:
  input:
    pon_db = f"{vcf__pipeTarget}/all.pon_db",
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict()
  output:
    pon = f"{vcf__pipeTarget}/all.pon.vcf.gz"
  run:
    genome_name = project__speciesGenomeName()
    if genome_name == "hs37d5":
      genome_name = "hg19"
    gatkDir = annot__gatkDir(sharedDir=True)
    germline_resource = f"af-only-gnomad.{genome_name}.vcf.gz"
    cmd = f"""
      export TILEDB_DISABLE_FILE_LOCKING=1
      gatk4 CreateSomaticPanelOfNormals \
       -R {input.ref_genome_fa} \
       --germline-resource {gatkDir}/{germline_resource} \
       -V gendb://{input.pon_db} \
       -O {output.pon}
       ln -s {output.pon} pon.vcf.gz
       ln -s {output.pon}.tbi pon.vcf.gz.tbi
    """
    exshell(**vars())

rule cnvkit_access:
  input:
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict()
  output:
    access = f"cnvkit_files/5kb_mappable.b3d"
  params:
    min_gap_size = f"5000"
  run:
    annotation_dir = annot__dir(sharedDir=True)
    genome_name = project__speciesGenomeName()
    if genome_name == "hs37d5":
      genome_name = "hg19"
    excludes_file = f"{genome_name}_excludes.bed"
    cmd = f"""
      mkdir -p cnvkit_files
      cnvkit.py \
      access \
      -s {params.min_gap_size} \
      -x {annotation_dir}/{excludes_file} \
      -o {output.access} \
      {input.ref_genome_fa}
    """
    exshell(**vars())

rule cnvkit_antitarget:
  input:
    access = f"cnvkit_files/5kb_mappable.b3d",
  output:
    antitargets = f"cnvkit_files/antitargets.b3d"
  run:
    targets = exome__baitIntervals(sharedDir=True)
    cmd = f"""
      cnvkit.py \
      antitarget \
      -g {input.access} \
      -o {output.antitargets} \
      {targets}
    """
    exshell(**vars())

rule cnvkit_coverage:
  input:
    sample_bam = f"{bam__sampleTarget}.bam",
    antitargets = f"cnvkit_files/antitargets.b3d"
  output:
    sample_targetcov = f"{vcf__sampleTarget}.targetcoverage.cnn",
    sample_antitargetcov = f"{vcf__sampleTarget}.antitargetcoverage.cnn"
  run:
    targets = exome__baitIntervals(sharedDir=True)
    if varcall__normalFilter(input.sample_bam.split(' ')):
      cmd = f"""
        cnvkit.py \
        coverage \
        -o {output.sample_targetcov} \
        {input.sample_bam} \
        {targets}
        cnvkit.py \
        coverage \
        -o {output.sample_antitargetcov} \
        {input.sample_bam} \
        {input.antitargets}
      """
    else:
      cmd = f"""
        touch {output.sample_targetcov} {output.sample_antitargetcov}
      """
    exshell(**vars())

rule cnvkit_reference:
  input:
    targetcovs = pypette.input(
             f"{vcf__sampleTarget}.targetcoverage.cnn",
             derefKwargs = ['sample_name',]),
    antitargetcovs = pypette.input(
             f"{vcf__sampleTarget}.antitargetcoverage.cnn",
             derefKwargs = ['sample_name',]),
    ref_genome_fa = lambda x: genome__ucscFasta(),
    ref_genome_fai = lambda x: genome__ucscFastaIdx(),
    ref_genome_dict = lambda x: genome__ucscFastaDict()
  output:
    reference = f"{vcf__pipeTarget}/all.reference.cnn"
  run:
    normal_targets = ' ' .join(varcall__normalFilter(map(str, input.targetcovs)) + varcall__normalFilter(map(str, input.antitargetcovs)))
    cmd = f"""
      cnvkit.py \
      reference \
      -f {input.ref_genome_fa} \
      -o {output.reference} \
      {normal_targets}
      ln -s ../{output.reference} cnvkit_files/reference.cnn
    """
    exshell(**vars())

rule cnvkit_batch:
  input:
    tumor_bam = f"{bam__sampleTarget}.bam"
  output:
    results = directory(f"{vcf__pipeTarget}/cnvkit_results")
  run:
    if varcall__selectMatch({input.tumor_bam}) == None:
      cmd = f"""
        mkdir -p {output.results}
        echo "This is a placeholder file for non-tumor samples." > {output.results}/normal_sample
      """
    else:
      if str(pypette.config.pipeline.name) == f"dna-wes":
        mode = "-m hybrid"
      if str(pypette.config.pipeline.name) == f"dna-wgs":
        mode = "-m wgs"
      reference = f"cnvkit_files/reference.cnn"
      cmd = f"""
      cnvkit.py \
      batch \
      {mode} \
      -r {reference} \
      -d {output.results}/ \
      --diagram \
      --scatter \
      {input.tumor_bam}
      """
    exshell(**vars())
