import os
import pandas as pd

# metadata should is a 2 columns tab-separated file with pairs of tumor and normal samples in each column
def varcall__selectMatch(sample):
  sample_bam = list(sample)[0]
  sample_name = os.path.splitext(os.path.basename(sample_bam))[0]
  df=pd.read_csv('samples/metadata.tsv', delimiter= '\t')
  tumor_samples = df['tumor'].values.tolist()
  normal_samples = df['normal'].values.tolist()
  try:
    tumor_idx = tumor_samples.index(sample_name)
    normal_match = normal_samples[tumor_idx]
    normal_bam = str(sample_bam).replace(sample_name, normal_match)
    return [normal_bam,normal_match]
  except ValueError:
    print("Non-tumoral sample, nothing to be done.")
    return None

def varcall__normalFilter(alltargets): 
  df=pd.read_csv('samples/metadata.tsv', delimiter= '\t')
  normal_samples = df['normal'].values.tolist()
  substr_normal = ["/" + normal_sample + "." for normal_sample in normal_samples]
  return [target for target in alltargets if any(normal in target for normal in substr_normal)]

