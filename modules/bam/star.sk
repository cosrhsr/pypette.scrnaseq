include: "star.py"


rule bam_star__aligned:
  """
  Renames STAR outputs to fit workflow conventions such as sample naming.
  Compresses STAR tab.
  """
  input:
    bam    = pypette.input(f"{bam_mapper__pipeTarget}/Aligned.sortedByCoord.out.bam"),
    logFin = pypette.input(f"{bam_mapper__pipeTarget}/Log.final.out"),
    tab    = pypette.input(f"{bam_mapper__pipeTarget}/SJ.out.tab"),
  output:
    bam    = pypette.temp(f"{bam_mapper__sampleTarget}.bam"),
    logFin = f"{bam_mapper__sampleTarget}.Log.final.out",
    tabgz  = f"{bam_mapper__pipeTarget}/SJ.out.tab.gz",
  run:
    cmd = f"""
      {hardlinkCmd(input.bam, output.bam)}
      {hardlinkCmd(input.logFin, output.logFin)}
      gzip -c {input.tab} > {output.tabgz}
    """
    exshell(**vars())

rule bam_star__alignReads:
  input:
    fqCheck = pypette.input(fastq__readsCheck),
    r1      = pypette.input(fastq__read, sample_read="R1") if pypette.pipeName != 'scrna' else [],
    r2      = pypette.input(fastq__read, sample_read="R2"),
    idxDir  = star__indexDir()
  output:
    bam       = pypette.temp(f"{bam_mapper__pipeTarget}/Aligned.sortedByCoord.out.bam"),
    tab       = pypette.temp(f"{bam_mapper__pipeTarget}/SJ.out.tab"),
    logOut    = f"{bam_mapper__pipeTarget}/Log.out",
    logPrOut  = f"{bam_mapper__pipeTarget}/Log.progress.out",
    logFinOut = f"{bam_mapper__pipeTarget}/Log.final.out",
    log       = f"{bam_mapper__pipeTarget}/star.log"
  run:
    star   = bam__configAligner()
    prefix = bam_mapper__pipeTarget.format(**wildcards) + os.path.sep
    reads  = star__readsToString(input.r1, input.r2)
    cmd = f"""
     {star.command}                                         \
       --runThreadN {star.cores}                            \
       --genomeDir {input.idxDir}                           \
       --readFilesIn {reads}                                \
       --outSAMstrandField intronMotif                      \
       --outFileNamePrefix {prefix}                         \
       --outSAMtype BAM SortedByCoordinate                  \
       --outSAMunmapped Within                              \
       --outFilterMismatchNmax {star.outFilterMismatchNmax} \
       --readFilesCommand zcat                              \
      > {output.log}
    """
    exshell(**vars())

rule bam_star__index:
  input:
    fa     = genome__ebiFasta(),
    gtf    = annot__ebiGtf()
  output:
    # Todo, link single files interanally to the directory to avoid touching 
    # of a file inside an output as a folder, when the output is a link to
    # a readonly folder.
    #idxDir = star__indexDir(),
    idxDir = directory(star__indexDir()),
  run:
    star   = bam__configAligner()
    cmd = f"""
      mkdir -p {output.idxDir}
      STAR                                   \
        --runThreadN {star.cores}            \
        --runMode genomeGenerate             \
        --genomeDir {output.idxDir}          \
        --genomeFastaFiles {input.fa}        \
        --outFileNamePrefix {output.idxDir}/ \
        --sjdbGTFfile {input.gtf}            \
        --sjdbOverhang 100                   \
        --genomeSAindexNbases 14
    """
    cmd, force = linkOrShareCmd(
      cmd,
      output.idxDir,
      isDir=True)
    exshell(**vars())

ruleorder: samples__runs > bam_star__aligned
