import pandas as pd
import os
import glob
from functools import reduce

pypette.includeModules("core/link.py")

def counts__mergedGenesDataFrameFromCounts(files):
  """ Merged all samples' gene counts dataframe """
  return counts__fmtColnames(
           reduce(
             pd.merge, 
             counts__genesDataFrames(files)))

def counts__genesDataFrames(files, compression='gzip'):
  """ Creates all samples' gene counts dataframes """
  return [ 
    pd.read_csv(f, delimiter='\t', skiprows=1, compression=compression)
    for f in files
  ]

def counts__fmtColnames(df):
  """ Formats the given dataframe's column names. """
  new = df.copy()
  new.columns = [ 
    colname.rstrip('.bam') 
    for colname in df.columns.map(os.path.basename) ]   
  return new

def counts__stdCols():
  return [ 'Geneid', 'Chr', 'Start', 'End', 'Strand', 'Length' ]

def counts__featureCountsStrandednessMap():
  return {
    'unstranded': 0,
    'forward'   : 1,
    'reverse'   : 2
  }

def counts__featureCountsStrandedness(strandedness):
  """
  Returns the expected strandedness for featureCounts.
  Strandedness is overwritten by pypette's --strandedness option.
  """
  try:
    return counts__featureCountsStrandednessMap()[counts__overwrittenStrandedness(strandedness)]
  except KeyError as ke:
    pypette.log.error(f"Unrecognized strandedness '{strandedness}'")
    sys.exit()

def counts__overwrittenStrandedness(strandedness):
  """
  Returns strand from pypette's --strandedness option if given.
  """
  if pypette.strandedness:
    strandedness = pypette.strandedness
  return strandedness

def counts__isPairStrand(strandedness):
  if counts__overwrittenStrandedness(strandedness) == 'unstranded':
    return False
  else:
    return True
