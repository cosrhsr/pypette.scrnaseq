import os
import datetime
import getpass
import socket
from collections import OrderedDict
from utils.files import fileSize
from utils.yaml import writeDict
from utils.strings import StringFormatter

pypette.includeModules("core/link.py")

def linkOrShareCmd(cmd, output, isDir=False):
  """
  Tries to link the given :output: if it exists in a shared folder. 
  First checks the shared datasets folder, then the user's datasets buffer folder.
  Otherwise, produces :output: from the given :cmd: command.
  Returns also a boolean for forcing the command execution in debug mode. 
  """

  """ No sharing of datasets """
  if not isDatasetShared():
    return cmd, False

  if os.path.exists(sharedDatasetPath(output)):
    """ Datasets available for sharing """
    sharedData = sharedDatasetPath(output, buffered=False)
    return (
      symlinkCmd(sharedData, output),
      True)
  else:
    sharedDataBuffer = sharedDatasetPath(output, buffered=True)
    if os.path.exists(sharedDataBuffer):
      """ Datasets is in buffer """
      return (
        symlinkCmd(sharedDataBuffer, output),
        True)
    else:
      """ Create datasets from scratch and send it to buffer """
      return (
        f"""
          {cmd}
          md5sum {output + "/*" if isDir else output} > {output}.md5
          mkdir -p {sharedDatasetDir(buffered=True)}
          cp -r --parent {output} {output}.md5 {sharedDatasetDir(buffered=True)}
          ln -sf {sharedDataBuffer} {output}
          echo {output} >> {sharedDatasetUserLog()}
        """, False)

def isDatasetShared():
  return pypette.config.cluster.sharedDatasets.use is True

def sharedDatasetPath(relpath, buffered=False):
  """
  Takes a relative path :relpath: and roots it to the right dataset base directory.
  """
  return os.path.join(sharedDatasetDir(buffered), relpath)

def sharedDatasetDir(buffered=False):
  if buffered:
    return os.path.join(
             pypette.config.cluster.sharedDatasets.bufferDir,
             getpass.getuser())
  else:
    return pypette.config.cluster.sharedDatasets.sharedDir

def sharedDatasetUserLog():
  """
  Generates the user's dataset log of files to share.
  """
  return os.path.join(sharedDatasetDir(buffered=True), "share.txt")

def exshell(cmd='', isSubCmd=False, force=False, **kwargs):
  """
  Formats the given command.
  Prepands the system's conda activation if specified in configuration.
  Prints the target's log if specified in the configuration.
  Executes the command if debug mode not set in configuration.
  """

  """ Separate workflow commands from stats """
  cmdLog  = cmd

  """ Write target's log """
  timeStart = datetime.datetime.now()
  writeCmdLog(cmdLog, timeStart=timeStart, **kwargs)
 
  if pypette.stats:
    cmdFull = cmdAddStats(cmdLog, **kwargs)
  else:
    cmdFull = cmdLog

  """ Protect Missing Keywords """
  cmdFull = StringFormatter(cmdLog).formatPartialMap(keepMissingKeys=True, protectKw=True, **kwargs)

  """ Debug mode """
  if config__valueOf('debug') and not force:
    touchOutputs(**kwargs)
  else:
    shell(cmdFull, **kwargs) 
  timeStop = datetime.datetime.now()

  """ Update target's log """
  writeCmdLog(cmdLog, timeStart=timeStart, timeStop=timeStop, **kwargs)

def touchOutputs(output, rule, **kargs):
  """
  Touches the given rule's outputs. Deals with regular files and directories.
  """
  ruleWfOutputs = list(filter(lambda x: x.name==rule, workflow.rules))[0].output

  outdirsWf  = list(filter(lambda x: x.is_directory, ruleWfOutputs))
  outdirs  = []
  outfiles = []
  for outdirWf in outdirsWf:
    outdirs += list(filter(lambda x: outdirWf.match(x), output))
  outfiles = list(filter(lambda x: x not in outdirs, output))

  list(map(lambda x: os.makedirs(x, exist_ok=True), outdirs))
  touch(outfiles)

# ------------
# Stats Logs
# ------------
def cmdAddStats(cmd='', **kwargs):
  return f"""
    {cmdStats(**kwargs)}
    {cmd}
  """

def cmdStats(**kwargs):
  #
  # CSV formatted stats Command
  #
  pidFile   = f"{targetName(**kwargs)}.pid"
  statsFile = f"{targetName(**kwargs)}.stats"
  interval  = '1'
  ret = [
    "echo $BASHPID > " + pidFile,
    "(S_TIME_FORMAT=ISO pidstat -u -r -d -h -p $(cat " + pidFile + ") " + interval \
     + " | tail -n +3 | sed -e '1s/^#//' -e '/^#\|^$/d' -e 's/^[ ]*//' -re 's/[[:blank:]]+/,/g'" \
     + " > " + statsFile + " &)"
  ]
  return os.linesep.join(ret)

# ------------
# Write Logs
# ------------
def writeCmdLog(cmd=None, logFile=None, **kwargs):
  if not config__valueOf('logCommands'):
    return
  logFile = f"{targetName(**kwargs)}.info.yaml"

  # TODO: Auto attribute missing fields in output. 
  # ?In which case is it needed?
  if 'sample_name' not in kwargs.keys():
    kwargs['sample_name'] = 'all'

  writeDict(logCmdDict(cmd, **kwargs), logFile)

def logCmdDict(cmds, **kwargs):
  """
  Sets the commands's OrderedDict for yaml export.
  """
  return OrderedDict({
    'rule'    : kwargs['rule'],
    'inputs'  : logFmtFilesYaml(kwargs['input']),
    'outputs' : logFmtFilesYaml(kwargs['output']),
    'commands': cmds.split(os.linesep),
    'commands': logFmtCmdYaml(cmds, **kwargs),
    'time'    : logCmdTimeDict(**kwargs),
    'mode'    : 'debugging' if config__valueOf('debug') else 'production',
    'host'    : socket.gethostname(),
  })

def logCmdTimeDict(**kwargs):
  return OrderedDict({
    'timeStart'  : str(kwargs['timeStart'])  if 'timeStart'  in kwargs.keys() else '---',
    'timeStop'   : str(kwargs['timeStop'] )  if 'timeStop'   in kwargs.keys() else '---',
    'elapsed'    : str(kwargs['timeStop'] - kwargs['timeStart']) if 'timeStart' in kwargs.keys() and 'timeStop' in kwargs.keys() else '---'
   })

def logFmtFilesYaml(files):
  """
  Format a list of :files: for yaml export.
  """
  return [
    f"{str(file)} ({fileSize(str(file))})"
    for file in files
  ]

def logFmtCmdYaml(cmds="", **kwargs):
  """
  Formats the given string of :cmds: for yaml export.
  """
  return [ strRmSpaces(cmd) for cmd in cmds.split(os.linesep) if cmd.strip()]

def strRmSpaces(words):
  """
  Removes blank spaces from the given string of :words:.
  """
  return ' '.join([ word.strip() for word in words.split(' ') if word.strip() ])

def targetName(**kwargs):
  """
  Gives a default basename for target's log.
  """
  if kwargs['output']:
    ret = f"{kwargs['output'][0]}"
  else:
    ret = kwargs['rule']
  return ret

