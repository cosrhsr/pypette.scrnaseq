pypette.includeModules("core/link.py")

## Todo: Check how to convert this function into Snakemake wrapper
def fastq_merging__mergeReadsCmd(input, output, useHardlink=False):
  """
  Concatenates the given fastq files if several. Links single fastq otherwise.
  """
  input.reads.sort()
  if len(input.reads) == 1:
    if useHardlink:
      cmd = hardlinkCmd(input.reads[0], output.read)
    else:
      cmd = symlinkCmd( input.reads[0], output.read)
  else:
    cmd = f"""
      cat {input.reads} > {output.read}
    """
  return cmd
