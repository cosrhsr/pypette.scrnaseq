#bash

project-rawdir ()
#
# Lists the first existing directory matching the project name.
# Reads from configuration file.
#
(
  yq .cluster.sequencingRuns.rawDirs < "${CLIP_OUTDIR}/config/cluster.yaml" \
  | grep '^ ' \
  | sed -e 's/,$//' -e 's/"//g' -e 's/[ ]*//g' \
  | xargs -I{} \ls -d {}/${CLIP_RUN}/${CLIP_PRJ} 2>/dev/null \
  | head -n 1
)
